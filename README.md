# Malibu - Anderson .Paak
![Album Cover](malibu.jpeg)
---
Malibu, released in 2016, is the second studio album by artist Anderson .Paak. The album earned .Paak his first two Grammy nominations for Best Progressive R&B Album and Best New Artist. The album is the perfect blend of Hip-Hop and soulful R&B that never gets old. .Paak's live performance of "Come Down", "Heart Don't Stand A Chance", and "Put Me Thru" on [NPR Music Tiny Desk Concert](https://www.youtube.com/watch?v=ferZnZ0_rSM) further emphasizes the brilliance of the album and artist.

## Songs
1. The Bird
2. Heart Don't Stand A Chance
3. The Waters
4. The Season/Carry Me
5. Put Me Thru
6. Am I Wrong
7. Without You
8. Parking Lot
9. Lite Weight
10. Room in Here
11. Waterfall (Interluuube)
12. Your Prime
13. Come Down
14. Silicon Valley
15. Celebrate
16. The Dreamer

## Top 3
- The Bird
- Put Me Thru
- Celebrate

## Features
- **BJ the Chicago Kid**
- **Schoolboy Q**
- **Rapsody**
- **The Free Nationals**
- **United Fellowship Choir**
- **The Game**
- **Sonyae Elise**
- **Talib Kweli**
- **Timan Family Choir**

## Nominations
- *Best Urban Contemporary Album*

## Charts
| Chart                     | Position |
| --------------------------| -------- |
| US Billboard 200          | 79       |
| US Independent Albums     | 9        |
| US Top R&B/Hip-Hop Albums | 9        |

> YES LAWWWWD!

